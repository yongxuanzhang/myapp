# myapp - an app demo to use Google Artifact Registry Upload Component.

This is a demo of using [Google Artifact Registry Upload Component](https://gitlab.com/yongxuanzhang/ar-upload).
Please check `.gitlab-ci.yml` for how the component is used. Open a new Merge Request can trigger the ci.
